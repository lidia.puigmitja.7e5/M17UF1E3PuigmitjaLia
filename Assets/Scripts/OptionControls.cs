using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionControls : MonoBehaviour
{

    [SerializeField] private Slider _sliderSpeed;
    [SerializeField] private Slider _sliderDistance;
    [SerializeField] private Slider _sliderCoolTime;

    private PlayerData _playerData;

    
    // Start is called before the first frame update
    void Start()
    {
        _sliderSpeed= GameObject.FindGameObjectWithTag("Speed").GetComponent<Slider>();
        _sliderDistance = GameObject.FindGameObjectWithTag("Distance").GetComponent<Slider>();
        _sliderCoolTime = GameObject.FindGameObjectWithTag("CoolTime").GetComponent<Slider>();
        _playerData = GameObject.Find("Player").GetComponent<PlayerData>();
        _sliderSpeed.onValueChanged.AddListener((v) => {
            _playerData.Speed = ((_playerData.Speed + v) / _playerData.Weight);
         });

        _sliderDistance.onValueChanged.AddListener((v) => {
            _playerData.Distancia = v;
        });

        _sliderCoolTime.onValueChanged.AddListener((v) => {
            _playerData.tempsRefredamentDefecte = v*20f;
        });
    }


    // Update is called once per frame
    void Update()
    {
      
        _sliderSpeed.onValueChanged.AddListener((v) => {
            _playerData.Speed =1+v;
        });

        _sliderDistance.onValueChanged.AddListener((v) => {
            _playerData.Distancia = v;
        });

        _sliderCoolTime.onValueChanged.AddListener((v) => {
            _playerData.tempsRefredamentDefecte = v * 20f;
        });
    }
}
