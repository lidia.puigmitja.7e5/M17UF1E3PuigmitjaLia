using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public enum PlayerState
{
    Gigant,
    Normal,
    Crecendo,
    Decrecendo

}

public enum PlayerMovement
{
    StopRight,
    StopLeft,
    WalkRight,
    WalkLeft,
    UserMoviment

}


public enum PlayerType
{
    Patata,
    Mago,
    Hada,
    Dragon,
    Guard

}


public class PlayerData : MonoBehaviour
{

    public PlayerState Player_state;
    public PlayerMovement Player_movement;
    public PlayerType Player_type;
    public string PlayerName;
    public float Height;
    public float Weight;
    public float Distancia;
    public float Speed;
    public float tempsHabilitat;
    public float tempsRefredamentDefecte;
    private GameObject _player;
    [SerializeField]
    
    private int _i = 0;
    private float _waitTime = 0f;



    [SerializeField]
    private Sprite[] _sprite;
    private SpriteRenderer _sr;

    // Nom,
    // Tipus de personatge,
    // al�ada, velocitat,
    // dist�ncia a rec�rrer. Aquest component ha de ser consultat per altres classes i editable des de l'inspector.

 
    private float _frame = 0;
    private float _framesTotal = 12;
    private float _tiempo;


    private Vector3 _minimScale = new Vector3(3f, 3f, 3f);
    private Vector3 _maximScale = new Vector3(9f, 9f, 9f);
    private Vector3 _changeScale = new Vector3(0.1f, 0.1f, 0.1f);
    private float tempsRefredament;
    [SerializeField]
    private float _totalSpeed;
 //   private float _speed;
   

    private void Awake()
    {
          _tiempo = Time.deltaTime;
        Distancia = 9f;
        _player = GameObject.Find("Player");
        Height = _player.transform.localScale.x;
        Weight = Height;
        Speed = 1;
        _totalSpeed = Speed / Weight;
        Player_movement = PlayerMovement.WalkLeft;
        tempsHabilitat = 10f;
        tempsRefredamentDefecte = 20f;
        tempsRefredament = tempsRefredamentDefecte;
        Player_type = PlayerType.Dragon;
    }



    // Start is called before the first frame update
    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        Player_state = PlayerState.Normal;


    }

    // Update is called once per frame
    void Update()
    {

        _tiempo += Time.deltaTime;
        Height = _player.transform.localScale.x;
        Weight = Height;
        _totalSpeed = Speed / Weight;

        if (_tiempo >= (1/_framesTotal))
        {
            _frame++;
            _tiempo = 0;

            if (_frame == 3)
            {
                if (_waitTime < 5f && (Player_movement==PlayerMovement.StopLeft || Player_movement==PlayerMovement.StopRight))
                {
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[0];
                    _frame = 0;
                    MovimentDistancia();
                }
                else
                {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = _sprite[_i];
                _i = _i < _sprite.Length - 1 ? (_i + 1) : 0;
                _frame = 0;
                   
                MovimentDistancia();
                ChangeState();
                    MoveNamePlayer();
                    MoveTypePlayer();
                
               
                }
             

            }
        }


    }

    void MovimentDistancia()
    {
        
        Quaternion targetR = Quaternion.Euler(0, 0f, 0f);
        Quaternion targetL = Quaternion.Euler(0, 180f, 0f);
        switch (Player_movement)
        {
            case PlayerMovement.WalkLeft:
                if (_player.transform.position.x > -Distancia)
                {
                    _player.transform.position = new Vector3((transform.position.x - _totalSpeed), transform.position.y, transform.position.z);
                }
                else
                {
                   
                    Player_movement = PlayerMovement.StopLeft;
                   
                }
                    break;

            case PlayerMovement.WalkRight:
                 if (_player.transform.position.x < Distancia)
                {
                    _player.transform.position = new Vector3((transform.position.x + _totalSpeed), transform.position.y, transform.position.z);
                }
                else
                {
                   
                    Player_movement = PlayerMovement.StopRight;
                    
                }
                break;

            case PlayerMovement.StopLeft:
                transform.rotation = targetL;
                if (_player.transform.position.x < -Distancia)
                {
                    _waitTime++;
                }
                
                if(_waitTime == 5f){

                    Player_movement = PlayerMovement.WalkRight;
                    _waitTime = 0f;
                }
               
                break;

            case PlayerMovement.StopRight:
                _player.transform.rotation = targetR;
                if (_player.transform.position.x > Distancia)
                {
                    _waitTime++;
                }

                if (_waitTime == 5f)
                {

                    Player_movement = PlayerMovement.WalkLeft;
                    _waitTime = 0f;
                }
                break;

            case PlayerMovement.UserMoviment:

                if (Input.GetAxis("Horizontal") < 0){
                    _player.transform.rotation = targetR;
                    transform.position = new Vector3(_totalSpeed * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z);
                }
                else
                {
                    _player.transform.rotation = targetL;
                    transform.position = new Vector3(_totalSpeed * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z);

                }
                
                break;

            default:
                break;
        }


        }

    void ChangeState()
    {
       
        switch (Player_state)
        {
            case PlayerState.Normal:
                _player.transform.localScale = _minimScale;
               

                break;

            case PlayerState.Gigant:
                _player.transform.localScale = _maximScale;
                tempsHabilitat -= 1;
                if (tempsHabilitat == 0f)
                {
                    Player_state = PlayerState.Decrecendo;
                    tempsHabilitat = 10f;
                }
                break;

            case PlayerState.Crecendo:
                if (_player.transform.localScale != _maximScale) _player.transform.localScale += _changeScale;
                if (_player.transform.localScale == _maximScale) Player_state = PlayerState.Gigant;

                break;

            case PlayerState.Decrecendo:
                if (_player.transform.localScale != _minimScale) _player.transform.localScale -= _changeScale;
                if (_player.transform.localScale == _minimScale)
                {
                    tempsRefredament -= 1;
                    if (tempsRefredament <= 0)
                    {
                         Player_state = PlayerState.Normal;
                        tempsRefredament = tempsRefredamentDefecte;
                    }
                 

                }
                break;


        }

    }


    private void MoveNamePlayer()
    {
        GameObject _text = GameObject.Find("PlayerNameFollow");
        Camera MainCamera = Camera.main;
        _text.GetComponent<TMP_Text>().text = PlayerName.ToString();
        _text.transform.position = MainCamera.WorldToScreenPoint(transform.position + new Vector3(0,(float)(transform.localScale.y-(transform.localScale.y-1.5)), 0));


    }
    private void MoveTypePlayer()
    {
        GameObject _textType = GameObject.Find("PlayerType");
        Camera MainCamera = Camera.main;
        _textType.GetComponent<TMP_Text>().text = Player_type.ToString();
        _textType.transform.position = MainCamera.WorldToScreenPoint(transform.position + new Vector3(0, (float)(transform.localScale.y-(transform.localScale.y-1)), 0));

    }


}
