using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TotalFrames : MonoBehaviour
{
    private float _tiempo;
    private void Awake()
    {
        _tiempo = 1/Time.deltaTime;
    }
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<TMP_Text>().text = _tiempo.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        _tiempo = 1 / Time.deltaTime;
        this.gameObject.GetComponent<TMP_Text>().text =_tiempo.ToString();
    }
}
