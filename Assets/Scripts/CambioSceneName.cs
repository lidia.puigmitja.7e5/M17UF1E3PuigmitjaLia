using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CambioSceneName : MonoBehaviour
{
    private TMP_Text _player;
    private Button _botton;
    // Start is called before the first frame update
    void Start()
    {

            _botton = GameObject.Find("Start").GetComponent<Button>();
            _botton.onClick.AddListener(TaskOnClick);
        

    }

    void TaskOnClick()
    {
        _player = GameObject.Find("Name").GetComponent<TMP_Text>();
        
   
        if (_player.text.Length>=3)
        {
            
            PlayerPrefs.SetString("name", _player.text);
            SceneManager.LoadScene("StartGame");
        }
     
     
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
