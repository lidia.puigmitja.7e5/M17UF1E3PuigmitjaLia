using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeGigant : MonoBehaviour
{

    private PlayerData _playerData;
    private GameObject _player;
    private Button _botton;

    // Start is called before the first frame update
    void Start()
    {
        _playerData = GameObject.Find("Player").GetComponent<PlayerData>();
        _player = GameObject.Find("Player");
        _botton = GameObject.Find("GrowUp").GetComponent<Button>();
       
    }

    private void TaskOnClick()
    {
        if (_playerData.Player_state == PlayerState.Normal)
        {
            _playerData.Player_state = PlayerState.Crecendo;
        }
       
    }



    // Update is called once per frame
    void Update()
    {
        _botton.onClick.AddListener(TaskOnClick);
       

    }
}
