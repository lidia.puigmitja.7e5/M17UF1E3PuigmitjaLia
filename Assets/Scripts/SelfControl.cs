using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelfControl : MonoBehaviour
{

    private PlayerData _playerData;
    private GameObject _player;
    private Button _botton;
    // Start is called before the first frame update
    void Start()
    {
        _playerData = GameObject.Find("Player").GetComponent<PlayerData>();
        _player = GameObject.Find("Player");
        _botton = GameObject.Find("TakeControl").GetComponent<Button>();
    }

    private void TaskOnClick()
    {
        if (_playerData.Player_movement != PlayerMovement.UserMoviment)
        {
            _playerData.Player_movement=PlayerMovement.UserMoviment;
        }
        else
        {
            if(_player.transform.position.x>_playerData.Distancia) _playerData.Player_movement = PlayerMovement.StopRight;
            else if (_player.transform.position.x < -_playerData.Distancia) _playerData.Player_movement = PlayerMovement.StopLeft;
            else if (_player.transform.position.x > -_playerData.Distancia) _playerData.Player_movement = PlayerMovement.WalkRight;

        }

    }

    // Update is called once per frame
    void Update()
    {
        _botton.onClick.AddListener(TaskOnClick);
    }
}
