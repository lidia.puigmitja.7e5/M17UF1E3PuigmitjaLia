using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;

public class MoveControl : MonoBehaviour
{

    private GameObject _player;
 

    // Start is called before the first frame update
    void Start()
    {

        _player = GameObject.Find("Player");



    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(_player.transform.position.x, transform.position.y, transform.position.z);

    }
}
