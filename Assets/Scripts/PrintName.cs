using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PrintName : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject _player;
    private PlayerData _playerData;
    private string _nombre;
    private void Awake()
    {
        _player = GameObject.Find("Player");
        _playerData = _player.GetComponent<PlayerData>();
        _playerData.PlayerName = PlayerPrefs.GetString("name");
    }

    void Start()
    { 
     // this.gameObject.GetComponent<Text>().text = GameObject.FindObjectOfType<PlayerData>().PlayerName;
       
        this.gameObject.GetComponent<TMP_Text>().text= _playerData.PlayerName;
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
